Module: Czech Audit
Author: Michal Cihar <michal@cihar.com>

This module is based by Google Analytics module by
Mike Carter <www.ixis.co.uk/contact>.


Description
===========

Adds the various Czech web site auditing systems to your website.

Requirements
============

You should have an accout on at least one of supported services.


Installation
============

* Copy the 'czech_audit' module directory in to your Drupal
modules directory as usual.


Usage
=====

In the settings page enter ID for services you want to use. Entries not
filled in will not be used.

You will also need to define what user roles should be tracked.
Simply tick the roles you would like to monitor.

All pages will now have the required JavaScript added to the
HTML footer can confirm this by viewing the page source from
your browser.

'admin/' pages are automatically ignored by Google Analytics.
